#!/usr/bin/env python3

#----------------------------#
#-------- MODULES -----------#
#----------------------------#

# Standard library module
import schedule
import sys
import time

# Local modules
import config
from modules.scheduled_task import ProgrammedMessages

#----------------------------#
#------- FUNCTIONS ----------#
#----------------------------#

def main():
    # Create an instance of the class
    obj_task = ProgrammedMessages(config.tlg_id, config.token)

    schedule.every(0.5).minutes.do(obj_task.scheduled_task)

    while True:
        try:
            schedule.run_pending()
            time.sleep(1)
        except Exception as e:
            print(e)
            sys.exit()

if __name__ == "__main__":
    main()
