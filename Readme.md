<h1> 🤖 Telegram Bot
<img src="images/chatbot.png"
       align="right" width="200" height="200" />
</h1>

This bot **translates english messages to spanish, and vice versa**, as well as sending **english audio pronunciations** of the requested word. 

Additionally, it can send scheduled messages/audios to a group or user.

### Files

- **Main programs**:
    - `main.py`: it translates english phrases to spanish, and vice versa, as well as sending english audio pronunciations of the requested word.
    - `test_scheduled_task.py`: it tests the **scheduled_task** module.
- **Configuration file**:
    - `config.py`: it contains global variables and constants.
- **Modules**:
    - `modules/translator.py`: it makes the calls to [mymemory](https://mymemory.translated.net/doc/spec.php) API to translate the messages.
    - `modules/scheduled_task.py`: it sends scheduled texts messages to a user/group.
- *Others*:
    - `audios/`: it contains two audio samples in *.ogg* format that will use **scheduled_task** module.

## Telegram API cheatsheet

- Get information about your bot:

```
https://api.telegram.org/bot[TELEGRAM_TOKEN]/getme
```

- Get messages received by your bot:

```
https://api.telegram.org/bot[TELEGRAM_TOKEN]/getupdates
```

- Send a message to the user whose ID is `[USER_ID]` with the text **hi**:

```
https://api.telegram.org/bot[TELEGRAM_TOKEN]/sendmessage?chat_id=[USER_ID]&text=hi
```
