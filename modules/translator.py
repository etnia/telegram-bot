import requests, json
import config

class Translator():

  def __init__(self):
    self.base = "https://api.mymemory.translated.net/"

  def get_word(self, word, original):
    if original is "ang":
      url = self.base + "get?q={}&langpair=en|es".format(word)
    elif original is "cas":
      url = self.base + "get?q={}&langpair=es|en".format(word)
    r = requests.get(url)
    response = json.loads(r.content)
    matches = response["matches"]
    list = []

    for x in matches:
      text_translation = x["translation"].lower()
      t = "- *" + x["segment"].lower() + "* " + config.BLACK_RIGHTWARDS_ARROW + " " + "_" + text_translation + "_"
      if t not in list and text_translation != word:
        print("word: ", word, "\ntranslation: ", t, "\n-------------------------------") # logs
        list.append(t)

    return config.LINE_BREAK.join(list)
