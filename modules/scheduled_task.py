#!/usr/bin/env python3

#----------------------------#
#-------- MODULES -----------#
#----------------------------#

# Standard library module
import time
import schedule
import os
import logging
import sys
from pathlib import Path

# Third-party modules
import telegram

#----------------------------#
#--------- LOGGING ----------#
#----------------------------#

# Enable logging
logging.basicConfig(stream=sys.stderr, level=logging.DEBUG)
logger = logging.getLogger(__name__)

#----------------------------#
#----- USER VARIABLES -------#
#----------------------------#

# Phrases Array
msg1 = "First inspirational message."
msg2 = "Second inspirational message."
msg3 = "Third inspirational message."

phrases_array = [msg1, msg2, msg3]

#----------------------------#
#--------- CLASS ------------#
#----------------------------#

class ProgrammedMessages():

    def __init__(self, tlg_id, token):
        self.tlg_id = tlg_id
        self.token = token
        self.bot = telegram.Bot(token)
        self.counter = 0
        self.has_finished_audios = False
        self.audios_array = self.absolute_audio_path()

    def absolute_audio_path(self):
        """
        Returns an array of the absolute paths of the audio files.
        """

        directory = str(Path(__file__).parent.parent) + "/audios"

        return [os.path.join(directory, file) for file in os.listdir(directory)]


    def scheduled_task(self):
        """
        Sends all audios (audios_array) if boolean has_finished_audios is set to False and then sends all custom messages (phrases_array).
        """

        # Logs
        logger.debug("Counter --> {}".format(self.counter))
        logger.debug("has_finished_audios --> {}".format(self.has_finished_audios))

        if not self.has_finished_audios:
          self.bot.send_audio(chat_id=self.tlg_id, audio=open(self.audios_array[self.counter], 'rb'))

          # Update counter
          self.counter += 1

          if self.counter == len(self.audios_array):
            self.has_finished_audios = True
            self.counter = 0

        else:
          self.bot.send_message(chat_id=self.tlg_id, text=phrases_array[self.counter])

          # Update counter
          self.counter = (self.counter + 1) % len(phrases_array) # It sends custom messages in a loop

if __name__ == "__main__":

    # Get object arguments
    tlg_id = input("Enter user or group ID: ")
    token = input("Enter telegram token: ")

    # Create an instance of the class
    obj_task = ProgrammedMessages(tlg_id, token)

    schedule.every(0.5).minutes.do(obj_task.scheduled_task)

    while True:
        try:
            schedule.run_pending()
            time.sleep(1)
        except Exception as e:
            print(e)
            sys.exit()
