import urllib.parse

#------------------#
# GLOBAL VARIABLES #
#------------------#

token = ''

list_authorized_ids = []
access_is_restricted = False

# Only used by test_ProgrammedMessages.py
tlg_id = '' # user or group id to send messages to
minutes_val = 0.5

#------------------#
# GLOBAL CONSTANTS #
#------------------#

# Emojis
BLACK_RIGHTWARDS_ARROW = u'\U000027A1'
FACE_WITH_PLEADING_EYES = u'\U0001F97A'
STUDIO_MICROPHONE = u'\U0001F399'
SMILING_FACE_WITH_HALO = u'\U0001F607'
THINKING_FACE = u'\U0001F914'
ROBOT_FACE = u'\U0001F916'
UNICORN_FACE = u'\U0001F984'
RAINBOW = '\U0001F308'
HEAR_NO_EVIL_MONKEY = u'\U0001F649'
PERSON_WITH_FOLDED_HANDS = u'\U0001F64F'

# URL encoding
LINE_BREAK = '%0A'
LINE_BREAK = urllib.parse.unquote(line_break)
