#!/usr/bin/env python3
# -*- coding: utf-8 -*-

#----------------------------#
#-------- MODULES -----------#
#----------------------------#

# Standard library modules
import subprocess
import re
import urllib.request
import random
import logging
import schedule
from functools import wraps

# Third-party modules
import telegram
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
from telegram import InlineQueryResultArticle, ParseMode, InputTextMessageContent
from telegram import ReplyKeyboardMarkup, ReplyKeyboardRemove
from wiktionaryparser import WiktionaryParser

# Local modules
import config
from modules.translator import Translator

#----------------------------#
#--------- LOGGING ----------#
#----------------------------#

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)

#----------------------------#
#----- INITIALIZATIONS ------#
#----------------------------#

tor = Translator()

#----------------------------#
#----- SYSTEM FUNCTIONS -----#
#----------------------------#

def restricted(func):
    @wraps(func)
    def wrapped(update, context, *args, **kwargs):
        chat_id = update.effective_user.id
        if chat_id not in config.list_authorized_ids and config.access_is_restricted: # If boolean access_is_restricted is True, then it checks the list of authorized ids
            print("Unauthorized access denied for {}".format(chat_id))
            return
        return func(update, context, *args, **kwargs)
    return wrapped

def error(update, context):
    """
    Log Errors caused by Updates.
    """
    logger.warning('Update "%s" caused error "%s"', update, context.error)

#----------------------------#
#--------FUNCTIONS ----------#
#----------------------------#

@restricted
def wakeUp(update, context):
    """
    It replies always the same text
    """
    context.bot.sendChatAction(chat_id = update.effective_chat.id, action = "typing")
    update.message.reply_text("Good morning! " + config.UNICORN_FACE + " How are you?")

@restricted
def ang(update, context):
    """
    If an english word or phrase is specified after /ang action, then it replies its spanish translation(s).
    """
    context.bot.sendChatAction(chat_id = update.effective_chat.id, action = "typing")
    action_arg = " ".join(context.args)

    if action_arg:
        reply = tor.get_word(word=action_arg, original="ang")
        if not reply: # If reply is empty
          reply = "What did you say? I need a valid english word!" + config.THINKING_FACE
    else:
        reply = "\[ENG " + config.BLACK_RIGHTWARDS_ARROW + " SPA]" + " You have to write what you want to translate after **/ang**."

    update.message.reply_text(reply, parse_mode=telegram.ParseMode.MARKDOWN)

@restricted
def cas(update, context):
    """
    If a spanish word or phrase is specified after /cas action, then it replies its english translation(s).
    """
    context.bot.sendChatAction(chat_id = update.effective_chat.id, action = "typing")
    action_arg = " ".join(context.args)

    if action_arg:
        reply = tor.get_word(word=action_arg, original="cas")
        if not reply: # If reply is empty
          reply = "What did you say? I need a valid spanish word!" + config.THINKING_FACE
    else:
        reply = "\[SPA " + config.BLACK_RIGHTWARDS_ARROW + " ENG]" + " You have to write what you want to translate after **/cas**."

    update.message.reply_text(reply, parse_mode=telegram.ParseMode.MARKDOWN)

@restricted
def pronounce(update, context):
    """
    If a single word is specified after /pronounce action, then it replies a set of audios pronouncing this word in different english accents if found, or just a single audio.
    """

    context.bot.sendChatAction(chat_id = update.effective_chat.id, action = "typing")

    if context.args: # If action has an argument

      if len(context.args) > 1:
          update.message.reply_text("I can only translate one word at a time! Searching for the first word..." + config.ROBOT_FACE)

      action_arg = context.args[0] # It only takes the first word in case there are more than one
      action_arg = action_arg.strip(',.!?¿-:;') # Strips all final special characters - useful for cut out phrases

      # Get information about the word on wiktionary in JSON
      parser = WiktionaryParser()
      word = parser.fetch(action_arg.lower())

      # Extract only URLs that correspond to audio files
      download_url = "https:"
      result = re.findall("\/\/[\w/\-?=%.]+\.[\w/\-?=%.]+", str(word))

      # Delete repeated elements in the list
      result = list(set(result))

      if result: # If it has gotten URLs

        for url_aux in result:

          # Get file name
          name = url_aux.split("/")
          name = name[len(name)-1]

          # Download audio file
          path = name
          new_path = name + 'temp.ogg'
          urllib.request.urlretrieve(download_url + url_aux, path)

          # Encode audio in opus codec
          subprocess.run(["ffmpeg", '-i', path, '-c:a', 'libopus', '-b:a', '96K', new_path], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

          # Send audio
          update.message.reply_voice(voice=open(new_path, 'rb'))

          # Delete the downloaded audio
          subprocess.run(["rm", path, new_path], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)

      else: # If it didn't get any URLs

        reply = "I'm sorry, I haven't been able to find a pronunciation for `" + action_arg + "`" + config.FACE_WITH_PLEADING_EYES
        update.message.reply_text(reply, parse_mode=telegram.ParseMode.MARKDOWN)

    else: # If action doesn't have an argument

      reply = "If you don't type a word, you won't listen my voice! " + config.STUDIO_MICROPHONE
      update.message.reply_text(reply)

@restricted
def message(update, context):
    reply = "Send me a task, please " + config.PERSON_WITH_FOLDED_HANDS
    update.message.reply_text(reply)

#----------------------------#
#--------- MAIN -------------#
#----------------------------#

def main():

    updater = Updater(config.token, use_context=True)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different command - answer in Telegram
    dp.add_handler(CommandHandler('ang', ang))
    dp.add_handler(CommandHandler('cas', cas))
    dp.add_handler(CommandHandler('pronounce', pronounce))
    dp.add_handler(CommandHandler('wakeUp', wakeUp))

    # on noncommand i.e message
    dp.add_handler(MessageHandler(Filters.text, message))

    # log all errors
    dp.add_error_handler(error)

    # start the bot
    updater.start_polling()

    # Block until the user presses Ctrl-C or the process receiver SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()

if __name__ == '__main__':
    main()
